# BitbucketKeyUploader

composer require gentle/bitbucket-api:^1.1.2 kriswallsmith/buzz:^0.16  guzzlehttp/guzzle

## Links
* http://docs.guzzlephp.org/en/stable/index.html
* https://gentlero.bitbucket.io/bitbucket-api/1.0/
* https://bitbucket.org/gentlero/bitbucket-api/src/master/
* https://developer.atlassian.com/bitbucket/api/2/reference/

To upload an sshkey we need to use an app password

https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html
curl -u myuser:myapppassword -X POST -H "Content-Type: application/json" -d '{"key": "key content goes here"}' https://api.bitbucket.org/2.0/users/myuser/ssh-keys
see test bash script for testing

Create an app password

To create an app password:


From your avatar in the bottom left, click Bitbucket settings.

Click App passwords under Access management.

Click Create app password.

Give the app password a name related to the application that will use the password.

Select the specific access and permissions you want this application password to have.
Copy the generated password and either record or paste it into the application you want to give access. The password is only displayed this one time

